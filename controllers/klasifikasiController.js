const {validationResult} = require('express-validator');
const conn = require('../dbConnection');
const klasifikasi = conn.firestore().collection('klasifikasi');

const addKlasifikasi = async(req, res) => {
    const errors = validationResult(req.body);
    if(!errors.isEmpty()){
        return res.status(422).json({ errors: errors.array() });
    }
    const {name, jenis_kelamin, umur, berat_badan} = req.body;    
    const inserted = await klasifikasi.add({ name: name , jenisKelamin: jenis_kelamin, umur: umur, beratBadan : berat_badan });   
    inserted.then(() => {
        res.status(201).json({ message:'input data klasifikasi berhasil' }); 
    }).catch((err) => {
        res.status(400).json({ message: err }); 
    })
}

const updateKlasifikasi = async(req, res) => {
    const {doc_id, name, jenis_kelamin, umur, berat_badan} = req.body;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errors: errors.array() });
    }
    const updated = await klasifikasi.doc(doc_id);
    if(updated){
        updated.update({ name: name, jenisKelamin: jenis_kelamin, umur: umur, beratBadan : berat_badan})
        .then(() => 
            res.status(201).json({ message: 'update data klasifikasi berhasil'})
        ).catch((err) =>
            res.status(400).json({ message: err })
        )
    }
}

const detailKlasifikasi = async(req, res) => {
    const name = req.query.name;
    if(name){
        await klasifikasi.where("name","==", name).get()
        .then((doc) => {
            if (doc.docs.length) {
                for (const dataklas of doc.docs) {
                    res.status(201).json({ message: "data result", data: dataklas.data() });                    
                }
            } else {
                res.status(404).json({ message: "data not found", data: {} });
            }
          }).catch((error) => {
            res.status(400).json({ message: error, data: {} });
        });
    }else{
        res.status(400).json({ message: "param not found", data: {} });
    }    
}

const deleteKlasifikasi = async(req, res) => {
    const doc_id = req.body.doc_id
    if(doc_id){
        await klasifikasi.doc(doc_id).delete()
        .then(() => {
            res.status(201).json({ message: "data klasifikasi terhapus" });                    
        }).catch((error) => {
            res.status(400).json({ message: error });
        });
    }else{
        res.status(400).json({message:'data klasifikasi not found'});
    }
}

module.exports = {addKlasifikasi, updateKlasifikasi, detailKlasifikasi, deleteKlasifikasi};