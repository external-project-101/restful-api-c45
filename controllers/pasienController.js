const {validationResult} = require('express-validator');
const conn = require('../dbConnection');
const pasien = conn.firestore().collection('pasien');

const addpasien = async(req, res) => {
    const errors = validationResult(req.body);
    if(!errors.isEmpty()){
        return res.status(422).json({ errors: errors.array() });
    }
    const {nama_ibu, nama_anak, jenis_kelamin, tgl_lahir} = req.body;    
    await pasien.add({ nama_ibu: nama_ibu, nama_anak: nama_anak, tanggal_lahir: tgl_lahir, jenis_kelamin: jenis_kelamin })
    .then(() => {
        res.status(201).json({ message:'input data pasien berhasil' }); 
    }).catch((err) => {
        res.status(400).json({ message: err }); 
    })
}

const updatepasien = async(req, res) => {
    const {doc_id, nama_ibu, nama_anak, jenis_kelamin, tgl_lahir} = req.body;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errors: errors.array() });
    }
    const updated = await pasien.doc(doc_id);
    if(updated){
        updated.update({ nama_ibu: nama_ibu, nama_anak: nama_anak, tanggal_lahir: tgl_lahir, jenis_kelamin: jenis_kelamin })
        .then(() => 
            res.status(201).json({ message: 'update data pasien berhasil'})
        ).catch((err) =>
            res.status(400).json({ message: err })
        )
    }
}

const detailpasien = async(req, res) => {
    const doc_id = req.query.doc_id;
    if(doc_id){
        await pasien.doc(doc_id).get()
        .then((doc) => {
            if (doc.exists) {
                res.status(201).json({ message: "data result", data: doc.data() });
            } else {
                res.status(404).json({ message: "data not found", data: {} });
            }
          }).catch((error) => {
            res.status(400).json({ message: error, data: {} });
        });
    }else{
        res.status(400).json({ message: "param not found", data: {} });
    }    
}

const deletepasien = async(req, res) => {
    const doc_id = req.body.doc_id
    if(doc_id){
        await pasien.doc(doc_id).delete()
        .then(() => {
            res.status(201).json({ message: "data pasien terhapus" });                    
        }).catch((error) => {
            res.status(400).json({ message: error });
        });
    }else{
        res.status(400).json({message:'data pasien not found'});
    }
}

const listpasien = async(req, res) => {
    await pasien.get()
    .then((doc) => {
        if (doc.docs.length) {
            const dataDocs = doc.docs.map((document) => {
                return { id: document.id, ...document.data() }
            })
            res.status(201).json({ message: "data result", data: dataDocs });  
        } else {
            res.status(404).json({ message: "data not found", data: {} });
        }                   
    }).catch((error) => {
        res.status(400).json({ message: error });
    });
}

module.exports = {addpasien, updatepasien, detailpasien, deletepasien, listpasien};