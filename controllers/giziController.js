const { validationResult } = require('express-validator');
const conn = require('../dbConnection');
const pasien = conn.firestore().collection('pasien');
const gizi_pasien = conn.firestore().collection('gizi_pasien');

function capFirstLetterInSentence(sentence) {
    let words = sentence.split(" ").map(word => {
        return word[0].toUpperCase() + word.slice(1);
    })
    return words.join(" ");
}

const checkGizi = async(req, res) => {
    const doc_name = req.query.name;
    if(doc_name){
        let docName = capFirstLetterInSentence(doc_name);
        await pasien.where("nama_anak","==", docName).get()
        .then((doc) => {
            if (!doc.empty) {
                doc.forEach(docs =>{
                    res.status(201).json({ message: "data result", data: docs.data() });
                })                
            } else {
                res.status(404).json({ message: "data not found", data: {} });
            }
          }).catch((error) => {
            res.status(400).json({ message: error, data: {} });
        });
    }else{
        res.status(400).json({ message: "param not found", data: {} });
    } 
}

const hitung = async(req, res) => {
    const tinggi = req.query.tinggi;
    const berat = req.query.berat;
    if (tinggi || berat){
        res.status(201).json({ message: 'data result', data: {} })
    }else{
        res.status(400).json({ message: "disabled parameter" })
    }
}

const updateGizi = async(req, res) => {
    const {nama_anak, berat_badan, tinggi_badan, usia, tanggal_check, imt, status_gizi} = req.body;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errors: errors.array() });
    }
    await gizi_pasien.add({ nama_anak: nama_anak, berat_badan: berat_badan, tinggi_badan: tinggi_badan, 
        usia: usia, tanggal_check: tanggal_check, indeks_massa: imt, status_gizi: status_gizi })
    .then(() => {
            res.status(201).json({ message: 'add data gizi pasien berhasil'})
    }).catch((err) => {
            res.status(400).json({ message: err })
    })
}
module.exports = {checkGizi , hitung, updateGizi};