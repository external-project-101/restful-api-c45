const router = require('express').Router();
const {body} = require('express-validator');
const gizi = require('./controllers/giziController');
const klasifikasi = require('./controllers/klasifikasiController');
const pasien = require('./controllers/pasienController');
const training = require('./controllers/trainingController');

// UNTUK PENGECEKAN GIZI
router.get('/gizi/checkgizi', gizi.checkGizi);
router.post('/gizi/hitung', gizi.hitung);
router.post('/gizi/update', gizi.updateGizi);

//UNTUK CRUD KLASIFIKASI - NOT USED
router.post('/klasifikasi/add', klasifikasi.addKlasifikasi);
router.post('/klasifikasi/update', [body('doc_id', "Please provide document id").notEmpty().escape().trim()], klasifikasi.updateKlasifikasi);
router.get('/klasifikasi/detail', klasifikasi.detailKlasifikasi);
router.post('/klasifikasi/delete', [body('doc_id', "Please provide document id").notEmpty().escape().trim()], klasifikasi.deleteKlasifikasi);

//UNTUK HANDLE TRAINING DATA
router.get('/training/master', training.generateGizi);
router.get('/training/generate', training.generateTraining);
router.get('/training/logistic', training.generateLogistic);
router.get('/training/tree', training.generateTree);

router.post('/pasien/add', pasien.addpasien);
router.post('/pasien/update', [body('doc_id', "Please provide document id").notEmpty().escape().trim()], pasien.updatepasien);
router.get('/pasien/detail', pasien.detailpasien);
router.post('/pasien/delete', [body('doc_id', "Please provide document id").notEmpty().escape().trim()], pasien.deletepasien);
router.get('/pasien/list', pasien.listpasien);

module.exports = router;